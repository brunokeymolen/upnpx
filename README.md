# **upnpx** - iOS/OSX Open Source UPnP Library#



> **June 2014**; upnpx is maintained by *[Felix Paul Kühne](http://feepk.net/)* ( [VideoLAN]([http://www.videolan.org) ) from now on. Please visit his fork at https://github.com/fkuehne/upnpx to get the latest status.


# Static OS X & iOS UPnP library written in Cocoa (UPnP) and C++ (SSDP).#

The stack API is 100% Cocoa (Objective C) and is designed to integrate easily into Mac OS X and iOS (iPhone and iPad) applications.

The Current implementation has support for control point/client only. 

**upnpx information**

[ Tutorial; upnpx API tutorial (build your own client in 6 steps)](https://bitbucket.org/brunokeymolen/upnpx/wiki/TutorialIndex)

 * [Libupnpx How to setup a project and use libupnpx.a to build your own iPhone UPnP client]
 * [TutorialNewDevice Add new Device & Service Specifications to the upnpx stack]
 
*interesting links*
 * http://upnp.org
 * *[https://github.com/fkuehne/upnpx upnpx fork by Felix Paul Kühne (VideoLAN)]*
 * https://code.google.com/p/upnpx/wiki/License 


* known applications that use upnpx *
<table border=0>
<tr>
<td>
<a href="https://itunes.apple.com/app/vlc-ios/id650377962">
<img src="https://dl.dropboxusercontent.com/u/23324693/upnpx/vlc.png"></img>
</a>
</td>

<td>        </td>

<td>
<a href="https://itunes.apple.com/us/app/sofaplay/id684463024">
<img src="https://dl.dropboxusercontent.com/u/23324693/upnpx/sofaplay.png"></img>
</a>
</td>
</tr>

<tr>
<td>
<a href="https://itunes.apple.com/app/vlc-ios/id650377962">
VLC 
by Felix Paul Kühne</a>
</td>
<td></td>
<td>
<a href="https://itunes.apple.com/us/app/sofaplay/id684463024">
!SofaPlay 
by Fabian Pimminger</a></td>
</tr>

</table>



*New*

2013.05.01: Merged in quite a number of changes made by the people of !SlingMedia [http://www.slingbox.com]. They added multithreaded support + memory leak fixes.

All credits and thanks for these changes go to: 

 * Jonathan Guan
 * Mujtaba Hassanpur 


<br>


*[LatestChanges Latest svn changes]*
 * For future upnpx development and releases, visit: https://github.com/fkuehne/upnpx

*Reference*

[http://www.keymolen.com/mediacloudv2.html MediaCloud v.2] : a GUI based on the sources of the upnpx stack. 
(well, I actually created upnpx out of `MediaCloud v.2`). It can serve as an example of the upnpx functionality.

[http://archive.keymolen.com http://archive.keymolen.com/img/mmscreenmedium.jpg]

<br>
[http://www.keymolen.com/mediacloud.html AppleTV UPnP Player]: a plugin that enables UPnP Video playback on the AppleTV Generation 1 (3.0.2).

[http://archive.keymolen.com/mediacloud.html http://archive.keymolen.com/img/atvmenu_small.png]


<br>

* [http://code.google.com/p/upnpx/source/browse/#svn%2Ftrunk%2Fprojects%2Fxcode3%2Fupnpxdemo%2Fupnpxdemo.xcodeproj upnpxdemo.xcodeproj] *

A small and simple iPhone demo application used for the [tutorial Tutorial].

http://upnpx.googlecode.com/svn/wiki/images/demo_screen_3.png




*Author*

http://www.keymolen.com

mail to : bruno.keymolen@gmail.com